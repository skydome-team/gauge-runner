from golang:alpine

RUN apk update && apk add git unzip wget gcc musl-dev curl

RUN curl -SsL https://downloads.gauge.org/stable | sh -s -- --location=/usr/bin

RUN gauge install go
RUN gauge install html-report

ENV PATH=$HOME/.gauge:$PATH



